import React from 'react';
import GridCell from './GridCell';
import Ship from './Ship';
import styled from 'styled-components';

const InitShipContainer = styled.div`
	width: 100%;
	height: 30vh;
	display: flex;
`;

const getGrid = () => {
	const rows = ['A', 'B', 'C', 'D', 'E', 'F'];
	const cols = [1, 2, 3, 4, 5, 6];
	return rows.map((row, i) => {
		return cols.map((col, j) => {
			return {
				id: `${row}${col}`,
				hasShip: false,
				hasSalvo: false,
				i,
				j
			};
		});
	});
};

const Container = styled.div`
	display: flex;
	flex-wrap: wrap;
`;

class SalvoGrid extends React.Component {
	state = {
		grid: getGrid(),
		ships: [
			{
				id: 1,
				type: 'submarine',
				size: '3',
				isLocated: false,
				orientation: 'horizontal'
			},
			{
				id: 2,
				type: 'boat',
				size: 2,
				isLocated: false,
				orientation: 'vertical'
			},
			{
				id: 3,
				type: 'destroyer',
				size: 5,
				isLocated: false,
				orientation: 'horizontal'
			}
		],
		locatedShips: []
	};

	placeShips = () => {
		console.log(this.state.locatedShips);
	};

	changeOrientation = ship => {
		const { ships, locatedShips } = this.state;
		if (!ship.isLocated) {
			this.setState({
				ships: ships.map(s => {
					if (s.id === ship.id) {
						return s.orientation === 'horizontal'
							? { ...s, orientation: 'vertical' }
							: { ...s, orientation: 'horizontal' };
					}
					return s;
				})
			});
		} else if (
			ship.isLocated &&
			this.checkSpaceWhenLocated(ship, ship.i, ship.j)
		) {
			const newShipLocations = this.getLocationsWhenLocated(ship);
			this.setState({
				grid: this.updateGridWithShipLocations(newShipLocations, ship.id),
				locatedShips: locatedShips.map(ls => {
					if (ls.id === ship.id) {
						return ls.orientation === 'horizontal'
							? {
									...ls,
									orientation: 'vertical',
									shipLocations: newShipLocations
							  }
							: {
									...ls,
									orientation: 'horizontal',
									shipLocations: newShipLocations
							  };
					}
					return ls;
				})
			});
		} else {
			alert('not enough space!');
			return;
		}
	};

	checkSpaceWhenLocated = (ship, iIndex, jIndex) => {
		if (!ship) return;
		const { grid } = this.state;
		if (ship.orientation === 'vertical') {
			if (grid[iIndex][jIndex + parseInt(ship.size) - 1]) {
				console.log('de vertical a horizontal');
				for (let offset = 1; offset < ship.size; offset++) {
					if (grid[iIndex][jIndex + offset].hasShip) {
						return false;
					}
				}
				return true;
			}
			return false;
		} else {
			console.log('de horizontal a vertical');
			if (grid[iIndex + parseInt(ship.size) - 1]) {
				for (let offset = 1; offset < ship.size; offset++) {
					if (grid[iIndex + offset][jIndex].hasShip) {
						console.log('tiene barco');
						return false;
					}
				}
				return true;
			}
			return false;
		}
	};

	checkSpace = (draggedShip, iIndex, jIndex) => {
		if (!draggedShip) return;
		const { grid } = this.state;
		if (draggedShip.orientation === 'horizontal') {
			if (grid[iIndex][jIndex + parseInt(draggedShip.size) - 1]) {
				for (let offset = 0; offset < draggedShip.size; offset++) {
					if (
						grid[iIndex][jIndex + offset].hasShip &&
						grid[iIndex][jIndex + offset].shipId !== draggedShip.id
					) {
						return false;
					}
				}
				return true;
			}
			return false;
		} else {
			if (grid[iIndex + parseInt(draggedShip.size) - 1]) {
				for (let offset = 0; offset < draggedShip.size; offset++) {
					if (
						grid[iIndex + offset][jIndex].hasShip &&
						grid[iIndex + offset][jIndex].shipId !== draggedShip.id
					) {
						return false;
					}
				}
				return true;
			}
			return false;
		}
	};

	updateGridWithShipLocations = (locations, shipId) => {
		console.log(shipId);
		const { grid } = this.state;
		return grid.map(row => {
			return row.map(col => {
				if (locations.includes(col.id)) {
					return {
						...col,
						hasShip: true,
						shipId
					};
				} else if (col.shipId === shipId && !locations.includes(col.id)) {
					return {
						...col,
						hasShip: false,
						shipId: null
					};
				} else {
					return col;
				}
			});
		});
	};

	updateData = droppedData => {
		if (!droppedData) return;
		const { i, j, droppedShip } = droppedData || {};
		const { grid, ships, locatedShips } = this.state;
		const shipLocations = [];
		for (let offset = 0; offset < droppedShip.size; offset++) {
			if (droppedShip.orientation === 'horizontal') {
				shipLocations.push(grid[i][j + offset].id);
			} else {
				shipLocations.push(grid[i + offset][j].id);
			}
		}
		if (!droppedShip.isLocated) {
			this.setState({
				grid: this.updateGridWithShipLocations(shipLocations, droppedShip.id),
				ships: ships.filter(ship => ship.id !== droppedShip.id),
				locatedShips: [
					...locatedShips,
					{ ...droppedShip, isLocated: true, shipLocations }
				]
			});
		} else {
			this.setState({
				grid: this.updateGridWithShipLocations(shipLocations, droppedShip.id),
				locatedShips: locatedShips.map(ls => {
					return ls.type === droppedShip.type
						? {
								...droppedShip,
								shipLocations
						  }
						: ls;
				})
			});
		}
	};

	getLocationsWhenLocated = ship => {
		if (!ship) return;
		const { grid } = this.state;
		const { i, j } = ship;
		const shipLocations = [];
		for (let offset = 0; offset < ship.size; offset++) {
			// is reversed
			if (ship.orientation === 'horizontal') {
				shipLocations.push(grid[i + offset][j].id);
			} else {
				shipLocations.push(grid[i][j + offset].id);
			}
		}
		return shipLocations;
	};

	render() {
		const { grid, ships, locatedShips } = this.state;
		return (
			<div>
				<h1>Salvo Grid</h1>
				<InitShipContainer>
					{ships.map(ship => (
						<Ship
							data={ship}
							key={ship.id}
							updateData={this.updateData}
							changeOrientation={() => this.changeOrientation(ship)}
						/>
					))}
				</InitShipContainer>
				<Container>
					{grid.map((row, i) => {
						return row.map((col, j) => {
							/* col: {id, hasShip, hasSalvo, i, j } */
							return (
								<GridCell data={col} key={col.id} checkSpace={this.checkSpace}>
									{locatedShips.map(ls => {
										if (ls.shipLocations[0] === col.id) {
											return (
												<Ship
													data={ls}
													key={ls.id}
													updateData={this.updateData}
													changeOrientation={() => this.changeOrientation(ls)}
												/>
											);
										} else return null;
									})}
								</GridCell>
							);
						});
					})}
				</Container>
				<button onClick={this.placeShips}>Place Ships</button>
			</div>
		);
	}
}

export default SalvoGrid;
