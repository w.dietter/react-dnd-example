import React from 'react';
import styled, { css } from 'styled-components';
import { useDrag } from 'react-dnd';

const colors = ['tomato', 'teal', 'lightgreen', 'purple', 'gold', 'orange'];

const located = css`
	position: absolute;
	top: 0;
	left: 0;
	z-index: 1;
	display: flex;
	width: 100px;
	height: 100px;

	&::before {
		content: '';
		position: absolute;
		background: ${props => colors[props.size]};
		z-index: 1;
		${props =>
			props.orientation === 'horizontal'
				? css`
						width: ${props => (props.size - 1) * 100 + '%'};
						height: 100%;
						top: 0;
						left: 100%;
				  `
				: css`
						width: 100%;
						height: ${props => (props.size - 1) * 100 + '%'};
						top: 100%;
						left: 0%;
				  `};
	}
`;

const notLocated = css`
	margin: 1rem;
	${props =>
		props.orientation === 'horizontal'
			? css`
					width: ${props => props.size * 100 + 'px'};
					height: 100px;
			  `
			: css`
					width: 100px;
					height: ${props => props.size * 100 + 'px'};
			  `}
`;

const ShipContainer = styled.div`
	box-sizing: border-box;
	${props => (props.isLocated ? located : notLocated)}
	visibility: ${props => (props.isDragging ? 'hidden' : 'visible')};
	background: ${props => colors[props.size]};
`;

export const ItemTypes = {
	SHIP: 'ship'
};

const Ship = ({
	data: { id, type, size, isLocated, orientation } = {},
	updateData,
	changeOrientation,
	removeShips
}) => {
	const [collectedProps, drag] = useDrag({
		item: {
			id: id,
			type: ItemTypes.SHIP,
			draggedShip: {
				size: size,
				type: type,
				id: id,
				isLocated: isLocated,
				orientation
			}
		},
		end(item, monitor) {
			const droppedData = monitor.getDropResult();
			updateData(droppedData);
		},
		collect: monitor => ({
			isDragging: monitor.isDragging(),
			didDrop: monitor.didDrop()
		})
	});
	const { isDragging } = collectedProps;
	return (
		<ShipContainer
			ref={drag}
			orientation={orientation}
			isDragging={isDragging}
			isLocated={isLocated}
			onClick={changeOrientation}
			size={parseInt(size)}
		/>
	);
};

export default Ship;
