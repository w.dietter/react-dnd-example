import React from 'react';
import SalvoGrid from './SalvoGrid';

const App = () => {
	return (
		<div>
			<SalvoGrid />
		</div>
	);
};

export default App;
