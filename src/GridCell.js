import React from 'react';
import styled from 'styled-components';
import Ship from './Ship';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './Ship';

const Cell = styled.div`
	position: relative;
	box-sizing: border-box;
	width: 100px;
	height: 100px;
	border: 1px dotted #eee;
	text-align: center;
	background-color: ${({ isOver }) =>
		isOver ? 'gold' : 'dodgerblue'};
	transition: background-color 200ms ease-in;
`;

const GridCell = ({
	data: { id, i, j, hasShip },
	checkSpace,
	children
}) => {
	const [collectedProps, drop] = useDrop({
		accept: ItemTypes.SHIP,
		drop(item, monitor) {
			return {
				// id of this cell -> "A1"
				id,
				i,
				j,
				droppedShip: { ...item.draggedShip, i, j }
			};
		},
		/* returns a boolean if the drag operation is allowed */
		canDrop(item, monitor) {
			return checkSpace(item.draggedShip, i, j);
		},
		collect: monitor => ({
			isOver: monitor.isOver({ shallow: true })
		})
	});

	const { isOver } = collectedProps;
	return (
		<Cell ref={drop} isOver={isOver}>
			{isOver ? 'is over' : id}
			{children}
		</Cell>
	);
};

export default GridCell;
